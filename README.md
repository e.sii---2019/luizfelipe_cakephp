# ENG_SOFT_cakePHP-2019-LUIZ_FELIPE_PICOLI

## Instalação

1. Download [Composer](https://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Run `php composer.phar create-project --prefer-dist cakephp/app [app_name]`.

Se o Composer estiver instalado globalmente, execute

```bash
composer create-project --prefer-dist cakephp/app
```

Caso você queira usar um nome de diretório de aplicativo personalizado (por exemplo, `/ myapp /`):

```bash
composer create-project --prefer-dist cakephp/app myapp
```

Agora você pode usar o servidor da sua máquina para exibir a página inicial padrão ou iniciar
o servidor web incorporado com:

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765` to see the welcome page.

## Update

Desde que este esqueleto é um ponto de partida para o seu aplicativo e vários arquivos
teria sido modificado de acordo com as suas necessidades, não há uma maneira de fornecer
atualizações automáticas, então você precisa fazer atualizações manualmente.

## Configuração

Leia e edite `config / app.php` e configure o` 'Datasources'` e qualquer outro
configuração relevante para sua aplicação.

## Layout

O esqueleto do aplicativo usa um subconjunto de [Foundation] (http://foundation.zurb.com/) (v5) CSS
framework por padrão. Você pode, no entanto, substituí-lo por qualquer outra biblioteca ou
estilos personalizados.

## clone

Para clonar o projoto:git clone git@gitlab.com:felipepicoli/eng_soft_cakephp-2019.git

## Crianção do Crud

-comendo para a instalação do projeto:

-composer create-project --prefer-dist cakephp/app [app_name]

-Feito isto ja foi criado seu projeto.

-Antes de criar o banco de dados tem quer fazer a conecção com o bancos de dados.

-EX:

-Vai em:c:\wamp64\www\exemplo\projeto01\config\app.php e altera este arquivo

-na linha 254 altera o campo

-username 'root'

-password ''

-datebase 'aula'

-Agora vai em localhost para criar o banco de dados.

-Agora execute o comando abaixo para a criação da tabelo no banco de dados.

-bin\cake bake all [nome da tabela]

Após a instalação do cakePHP vai gerar o projeto onde você pode começar criar seu site.Abre o promt de comendo do Windows navega ate o seu projeto.
Agora com CMD do windows entra no projeto e navega ate  pasta bin Feito isto cria um banco de dados no localhost phpmyadmin e depois vai promt comento do windows e colocar este comando
cake base all e mais o nome do banco de dados que voce criou.Feito voce ja criou seu CRUD.

## Video sobre instalação e configuração do CakePHP

https://youtu.be/mg-GxgY5ktk


